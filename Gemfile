source 'http://rubygems.org'

# Heroku uses the ruby version to configure your application's runtime.
ruby '2.0.0'

gem 'rails', '4.0.0'

gem 'unicorn', require: false # Use unicorn as the app server
gem 'thin', require: false
gem 'sass-rails', '~> 4.0.0' # Use SCSS for stylesheets


#gem 'devise'               # for authentication
#gem 'cancan'               # for authorization
#gem 'rails_admin'

gem 'uglifier', '>= 1.3.0' # Use Uglifier as compressor for JavaScript assets
gem 'coffee-rails', '~> 4.0.0' # Use CoffeeScript for .js.coffee assets and views
gem 'therubyracer', platforms: :ruby # See https://github.com/sstephenson/execjs#readme for more supported runtimes
gem 'libv8', '~> 3.11.8'
gem 'jquery-rails' # Use jquery as the JavaScript library
gem 'jquery-ui-rails'
gem 'turbolinks' # Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'jbuilder', '~> 1.2' # Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'rails_12factor'

# assets
gem 'slim-rails'
gem 'less-rails'
gem 'less-rails-bootstrap', '<3.0.0'
gem 'nprogress-rails'

group :doc do
  # bundle exec rake doc:rails generates the API under doc/api.
  gem 'sdoc', require: false
end

gem 'awesome_print'        # pretty prints Ruby objects in full color
gem 'figaro'               # used for keeping secret data private

gem 'debugger', group: [:development, :test] # Use debugger

group :test, :development do
  gem 'sqlite3'
 # gem 'railroady'

# guard
  gem 'guard'              # automatically run various tasks depending on file changes
  gem 'guard-annotate'     # annotates model classes on schema changes
end

group :development do
  gem 'rails-erd'          # generates ERD from model (`rake erd`)
  gem 'ruby-graphviz'      # for state_machine graphs

  gem 'pry-rails'          # (more than) an IRB replacement
  #gem 'rack-mini-profiler' # displays profiler in left upper corner
  gem 'foreman'
  gem 'quiet_assets'
                           # debugging
  gem 'better_errors'      # shows verbose error messages if action fails
  gem 'binding_of_caller'  # provides additional info for better_errors
  gem 'meta_request'       # for Chrome RailsPanel Extension

  gem 'guard-livereload'   # reloads the browser after each change (browser plugin is required)
  gem 'rb-fsevent'
  gem 'libnotify'          # for guard notification
end

gem 'simple_form'

group :production do
  gem 'pg'
end
