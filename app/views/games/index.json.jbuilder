json.array!(@games) do |game|
  json.extract! game, :name, :map_id
  json.url game_url(game, format: :json)
end
