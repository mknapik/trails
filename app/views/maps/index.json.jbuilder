json.array!(@maps) do |map|
  json.extract! map, :min, :width, :height
  json.url map_url(map, format: :json)
end
