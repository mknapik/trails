json.array!(@algorithms) do |algorithm|
  json.extract! algorithm, :bot
  json.url algorithm_url(algorithm, format: :json)
end
