class GameEngine
  # @param [Game] game game model
  def initialize(game)

    @game = game

    game_map = game.map
    @rounds_num = game.rounds_num
    algorithms = game.algorithms

    if algorithms.length < game_map.min
      raise "Invalid algorithms number: #{algorithm.length}"
    end

    @player_states = []

    algorithms.each { |algo|
      @player_states << PlayerState.new(algo)
    }

    # assign number of player on map
    @player_states.length.times { |i|
      @player_states[i].number = i
    }

  end

  def start
    @rounds_num.times { |r_number|
      puts "Game #{@game.name} round #{r_number}"

      round = Round.new
      round.number = r_number
      @game.rounds << round
      @game.status = 'started'
      @game.save!

      @player_states.each {|p| p.alive = true }

      height = @game.map.height
      width = @game.map.width

      map_state = Array.new(height) { Array.new(width) { '  ' } }

      width.times { |x|
        map_state[0][x] = 'XX'
        map_state[height-1][x] = 'XX'
      }

      height.times { |y|
        map_state[y][0] = 'XX'
        map_state[y][width-1] = 'XX'
      }

      @game.map.start_points.shuffle # NOTICE

      directions = [:up, :down, :left, :right]

      # randomized starting point on start of each round
      @player_states.count.times { |i|
        p = @player_states[i]
        p.x = @game.map.start_points[i].x
        p.y = @game.map.start_points[i].y
        map_state[p.y][p.x] = "P#{p.number}"

        p.direction = directions[rand(4)]

        Rails.logger.info("Player #{p.number} has direction #{p.direction}")
      }

      step_i = 0

      step = Step.new
      step.number = step_i
      round.steps << step
      round.save!

      # zapis kroków początkowych
      @player_states.each { |p|
        as = AlgorithmStep.new
        as.score = p.score
        as.x = p.x
        as.y = p.y
        step.algorithm_steps << as
        p.algorithm.algorithm_steps << as
        p.algorithm.save!
      }

      step.save!

      # TODO
      print_map(map_state)

      almap = @player_states.map {|p| p.alive}
      Rails.logger.info("begin #{almap}")

      # pętla KROKU (tury)
      while (@player_states.select {|p| p.alive}).length > 1

        step = Step.new

        # execute step
        (@player_states.select { |p| p.alive}).each { |p|

          p.next_position(map_state)

          # check if player survive

          # for each algorithm compute step, move players, remove dead players
        }

        # stos do zabicia
        to_del = []


        almap = @player_states.map {|p| p.alive}
        Rails.logger.info("before #{almap}")

        # każdy gracz ma nową pozycję - sprawdź, czy przeżyje
        (@player_states.select { |p| p.alive}).each { |p|

          n_state =
              case map_state[p.y][p.x]
                when /XX/ then
                  :dead
                when /W(\d+)$/ then # TODO: kilka cyfr?
                  p_num = $1[1].to_i
                  if p_num != p.number
                    @player_states[p_num].score += 1
                  end
                  :dead
                when /P(\d+)$/ then
                  p_num = $1[1].to_i
                  if p_num != p.number # niemożliwe?
                    @player_states[p_num].score += 1
                  end
                  :dead
                when /  $/ then
                  # zderzenie w jedno pole
                  tmp = :alive
                  (@player_states.select { |pother| pother.alive and pother.number != p.number}).each { |pother|
                    if(p.x == pother.x and p.y == pother.y)
                      pother.score += 1
                      tmp = :dead
                    end
                  }

                  tmp
              end

          # wrzuć zabitych na stos do usunięcia
          if n_state == :dead
            to_del << p
          end

        }

        almap = @player_states.map {|p| p.alive}
        Rails.logger.info("after #{almap}")

        Rails.logger.info("#{to_del}")

        # remove dead
        to_del.each { |p|
          p.x = -1
          p.y = -1
          p.alive = false
          p.score -= 1
          p.remove(map_state)
        }

        # move player and save their steps
        (@player_states.select { |p| p.alive or to_del.include?(p)}).each { |p|

          Rails.logger.info("player #{p.number} algo step #{step_i} save")

          map_state[p.y][p.x] = "P#{p.number}" if p.alive

          as = AlgorithmStep.new
          as.score = p.score
          as.x = p.x
          as.y = p.y

          step.algorithm_steps << as

          p.algorithm.algorithm_steps << as
          p.algorithm.save!

          #as.save!
        }

        # TODO
        print_map(map_state)

        # save this step (FOR ALL PLAYERS) result (turn)
        step.number = step_i
        step.save!

        # update round in db
        round.steps << step
        round.save!

        @game.rounds << round
        @game.save!

        step_i += 1
      end

      @player_states.each { |p|
        p.score = 0
      }

      }

    @game.status = 'finished'
    @game.save!
  end

  def print_map(map_state)
    height = map_state.length
    width = map_state[0].length

    height.times { |y|
      width.times { |x|
        print map_state[y][x]
      }
      puts ''
    }
  end

end
