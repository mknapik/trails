class PlayerState
  def initialize(algorithm)
    @x = 0
    @y = 0
    @direction = :up # TODO initialize

    @score = 0
    @algorithm = algorithm
    @alive = true
    @number = -1

    raise 'No algorithm provided' if not @algorithm

    # TODO: name of method: process_step
    # TODO: params: array, player position x, y
    # TODO: spradzić syntax
    PlayerState.class_eval(@algorithm.code) # zakładamy, że dobrze składniowo
  end

  def algorithm
    @algorithm
  end

  def direction=(d)
    @direction=d
  end

  def direction
    @direction
  end

  def x
    @x
  end

  def y
    @y
  end

  def x=(v)
    @x = v
  end

  def y=(v)
    @y = v
  end

  def score=(s)
    @score = s
  end

  def score
    @score
  end

  def number
    @number
  end

  def number=(n)
    @number = n
  end

  def alive=(a)
    @alive = a
  end

  def alive
    @alive
  end

  def next_position(map_state)
    map_state[@y][@x] = "W#{@number}"
    turn = process_step(map_state, @x, @y, @direction)

    case @direction
      when :up then
        case turn
          when :forward then
            @y += 1
          when :right then
            @x += 1
            @direction = :right
          when :left then
            @x -= 1
            @direction = :left
        end
      when :right then
        case turn
          when :forward then
            @x += 1
          when :right then
            @y -= 1
            @direction = :down
          when :left then
            @y += 1
            @direction = :up
        end
      when :down then
        case turn
          when :forward then
            @y -= 1
          when :right then
            @x -= 1
            @direction = :left
          when :left then
            @x += 1
            @direction = :right
        end
      when :left then
        case turn
          when :forward then
            @x -= 1
          when :right then
            @y += 1
            @direction = :up
          when :left then
            @y -= 1
            @direction = :down
        end
    end
  end

  def remove(map_state)
    height = map_state.length
    width = map_state[0].length

    width.times { |x|
      height.times { |y|
        if map_state[y][x] == "W#{@number}"
          map_state[y][x] = '  '
        end
      }

    }
  end

end
