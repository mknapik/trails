class AlgorithmsController < ApplicationController
  before_action :set_algorithm, only: [:show, :edit, :update, :destroy, :remove]
  before_action :set_game, only: [:create, :edit, :new, :update, :show, :remove]

  # GET /algorithms
  # GET /algorithms.json
  def index
    @algorithms = Algorithm.all
  end

  # GET /algorithms/1
  # GET /algorithms/1.json
  def show
  end

  # GET /algorithms/new
  def new
    @algorithm = Algorithm.new
  end

  # GET /algorithms/1/edit
  def edit
  end

  # POST /algorithms
  # POST /algorithms.json
  def create
    ap @game
    @algorithm = Algorithm.new(algorithm_params)
    @algorithm.games << @game

    respond_to do |format|
      if @algorithm.save
        format.html { redirect_to game_path(@game), notice: 'Algorithm was successfully created.' }
        format.json { render action: 'show', status: :created, location: @algorithm }
      else
        ap @algorithm
        format.html { render action: 'new' }
        format.json { render json: @algorithm.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /algorithms/1
  # PATCH/PUT /algorithms/1.json
  def update
    respond_to do |format|
      if @algorithm.update(algorithm_params)
        format.html { redirect_to @game, notice: 'Algorithm was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @game.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /algorithms/1
  # DELETE /algorithms/1.json
  def destroy
    @algorithm.destroy
    respond_to do |format|
      format.html { redirect_to game_algorithms_url }
      format.json { head :no_content }
    end
  end

  def remove
    @game.algorithms.delete(@algorithm)
    redirect_to game_path(@game), :notice => 'Algorithm was removed from the game'
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_algorithm
    @algorithm = Algorithm.find(params[:id])
  end

  def set_game
    @game = Game.find(params[:game_id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def algorithm_params
    params.require(:algorithm).permit(:bot, :code, :description)
  end
end
