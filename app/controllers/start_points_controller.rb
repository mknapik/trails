class StartPointsController < ApplicationController
  before_action :set_start_point, only: [:show, :edit, :update, :destroy]
  before_action :set_map

  # GET /start_points
  # GET /start_points.json
  def index
    @start_points = StartPoint.where(map_id: @map.id)
  end

  # GET /start_points/1
  # GET /start_points/1.json

  # GET /start_points/new
  def new
    @start_point = StartPoint.new
  end

  # GET /start_points/1/edit
  def edit
  end

  # POST /start_points
  # POST /start_points.json
  def create
    @start_point = StartPoint.new(start_point_params)
    @start_point.map = @map

    respond_to do |format|
      if @start_point.save
        format.html { redirect_to map_start_points_path(@map), notice: 'start_point was successfully created.' }
        format.json { render action: 'show', status: :created, location: @start_point }
      else
        format.html { render action: 'new' }
        format.json { render json: @start_point.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /start_points/1
  # PATCH/PUT /start_points/1.json
  def update
    respond_to do |format|
      if @start_point.update(start_point_params)
        format.html { redirect_to map_start_points_path(@map), notice: 'start_point was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @start_point.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /start_points/1
  # DELETE /start_points/1.json
  def destroy
    @start_point.destroy
    respond_to do |format|
      format.html { redirect_to map_start_points_path(@map) }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_start_point
      @start_point = StartPoint.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def start_point_params
      params.require(:start_point).permit(:x, :y)
    end

    def set_map
      @map = Map.find(params[:map_id])
    end
end
