class RoundsController < ApplicationController
  before_action :set_game

  def show
    round = Round.find(params[:id])
    map = @game.map
    @steps = Step.where(round: round).includes(:algorithm_steps).all

    @map = (0 .. map.width - 1).map { |y|
      (0.. map.height - 1).map { |x|
        cell = {}
        cell['x'] = x
        cell['y'] = y
        cell
      }
    }
  end

  private

  def set_game
    @game = Game.find(params[:game_id])
  end

end
