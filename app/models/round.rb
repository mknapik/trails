class Round < ActiveRecord::Base
  belongs_to :game
  has_many :steps

  def winner
    max_points = -999999
    current = nil
    scores.each { |k,v|
      if v > max_points
        current = k
        max_points = v
      end
    }
    current
  end

  def scores
    steps_sorted = steps.sort_by { |s| -s.number }
    player_scores = {}
    steps_sorted.each { |s|
      s.algorithm_steps.each { |as|
        if not player_scores.include?(as.algorithm)
          player_scores[as.algorithm] = as.score
        end
      }
    }
    player_scores
  end

end
