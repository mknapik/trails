class AlgorithmStep < ActiveRecord::Base
  belongs_to :algorithm
  belongs_to :step
end
