class StartPoint < ActiveRecord::Base
  belongs_to :map

  validates :x, :y, presence: true
  validates :x, uniqueness: {scope: [:map, :y]}
end
