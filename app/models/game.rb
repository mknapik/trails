class Game < ActiveRecord::Base
  belongs_to :map
  has_many :rounds

  has_and_belongs_to_many :algorithms

  validates :map,
            presence: true
  validates :name,
            presence: true,
            uniqueness: true,
            allow_blank: false
  validates :status, inclusion: { in: %w(new started finished)}
  validates :rounds_num, presence: true, numericality: {greater_than: 0}

  def active?
    status == 'new'
  end

  def winner
    max_points = -999999
    current = nil
    scores.each { |k,v|
      if v > max_points
        current = k
        max_points = v
      end
    }
    current
  end

  def scores
    player_scores = {}

    algorithms.each { |a|
      player_scores[a] = 0
    }

    rounds.each { |r|
      round_scores = r.scores
      round_scores.each { |k,v|
        player_scores[k] += v
      }
    }

    player_scores
  end

  def has_min_players?
    self.algorithms.count >= self.map.min
  end

  def map_full?
    self.algorithms.count >= self.map.max
  end
end
