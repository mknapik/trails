class Step < ActiveRecord::Base
  belongs_to :round
  has_many :algorithm_steps
  has_many :algorithms, through: :algorithm_steps
end
