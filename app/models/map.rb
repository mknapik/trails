class Map < ActiveRecord::Base
  has_many :games
  has_many :start_points

  validates :min, :width, :height,
            presence: true
  validates :width, numericality: {greater_than: 3}
  validates :height, numericality: {greater_than: 3}

  def prettyname
    "min: #{min}\t max: #{max}\t width: #{width}\t height: #{height}"
  end

  def start_points_valid?
    self.start_points.count >= self.min
  end

  def max
    start_points.count
  end
end
