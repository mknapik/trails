// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require twitter/bootstrap
//= require_tree .


function goToStep(nr) {
    $('.marked').attr('class', "");
    for (var i = 0; i < nr; i++) {
        doStep(i);
    }
}

function doStep(nr) {
    var currentStep = steps[nr];
    var firstStep = steps[0]

    $('.current-step').removeClass("current-step")
    $('.dead').removeClass("dead")

    var i = 0;
    for (var player_nr in firstStep) {
        i++;
        $('.player-' + player_nr).html(" ");
        var step = currentStep[player_nr];
        if (!step || step.x < 0) {
            $('.player-' + i).addClass("dead")
        } else {
            var sel = "td[x='" + step.x + "'][y='" + step.y + "']";

            $(sel).addClass("marked player-" + i).addClass('current-step');
        }
    }
}