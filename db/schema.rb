# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20131204073926) do

  create_table "algorithm_steps", force: true do |t|
    t.integer  "x"
    t.integer  "y"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "score"
    t.integer  "step_id"
    t.integer  "algorithm_id"
  end

  create_table "algorithms", force: true do |t|
    t.boolean  "bot"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "code"
    t.string   "name"
    t.string   "description"
  end

  create_table "algorithms_games", primary_key: "false", force: true do |t|
    t.integer "game_id"
    t.integer "algorithm_id"
  end

  create_table "games", force: true do |t|
    t.string   "name"
    t.integer  "map_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "status",     default: "new", null: false
    t.integer  "rounds_num", default: 1,     null: false
  end

  add_index "games", ["map_id"], name: "index_games_on_map_id"

  create_table "maps", force: true do |t|
    t.integer  "min"
    t.integer  "width"
    t.integer  "height"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "rounds", force: true do |t|
    t.integer  "number"
    t.integer  "game_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "rounds", ["game_id"], name: "index_rounds_on_game_id"

  create_table "start_points", force: true do |t|
    t.integer  "map_id"
    t.integer  "x"
    t.integer  "y"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "start_points", ["map_id"], name: "index_start_points_on_map_id"

  create_table "steps", force: true do |t|
    t.integer  "number"
    t.integer  "round_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "steps", ["round_id"], name: "index_steps_on_round_id"

end
