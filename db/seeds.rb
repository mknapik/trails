# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Dir.entries("db/bots").select { |stooge|
  stooge != '.' && stooge != '..'
}.each { |stooge|
  code =  File.read("db/bots/" + stooge)
  name = stooge[0..-4]
  5.times{
    |x|   Algorithm.where(:bot => true, :name => (name + x.to_s))
        .first_or_create().update(:code => code, :description => (name + " nr: " +x.to_s))
  }
}

Map.where(:id => 2).first_or_create().update(:min => 2, :width => 12, :height => 12)

[3, 6, 9].map{
  |x| [3, 6, 9].map{
    |y|  StartPoint.where(:map_id => 2, :x => x, :y => y).first_or_create()
  }
}


