def process_step(gmap, x, y, direction)

  if(direction == :up)
    if(gmap[x][y+1] == "  ")
      return :forward
    end
    if(gmap[x+1][y] == "  ")
      return :right
    end
    if(gmap[x-1][y] == "  ")
      return :left
    end
  end


  if(direction == :down)
    if(gmap[x][y-1] == "  ")
      return :forward
    end
    if(gmap[x-1][y] == "  ")
      return :right
    end
    if(gmap[x+1][y] == "  ")
      return :left
    end
  end

  if(direction == :right)
    if(gmap[x+1][y] == "  ")
      return :forward
    end
    if(gmap[x][y-1] == "  ")
      return :right
    end
    if(gmap[x][y+1] == "  ")
      return :left
    end
  end


  if(direction == :left)
    if(gmap[x-1][y] == "  ")
      return :forward
    end
    if(gmap[x][y+1] == "  ")
      return :right
    end
    if(gmap[x][y-1] == "  ")
      return :left
    end
  end

  return :forward
end