class CreateAlgorithmSteps < ActiveRecord::Migration
  def change
    create_table :algorithm_steps do |t|
      t.integer :x
      t.integer :y

      t.timestamps
    end
  end
end
