class CreateAlgorithmsGames < ActiveRecord::Migration
  def change
    create_table :algorithms_games, primary_key: false do |t|
      t.references :game
      t.references :algorithm
    end
  end
end
