class CreateSteps < ActiveRecord::Migration
  def change
    create_table :steps do |t|
      t.integer :number
      t.references :round, index: true

      t.timestamps
    end
  end
end
