class CreateMaps < ActiveRecord::Migration
  def change
    create_table :maps do |t|
      t.integer :min
      t.integer :width
      t.integer :height

      t.timestamps
    end
  end
end
