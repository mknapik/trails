class AddCodeToAlgorithm < ActiveRecord::Migration
  def change
    add_column :algorithms, :code, :text
  end
end
