class AddDescriptionToAlgorithm < ActiveRecord::Migration
  def change
    add_column :algorithms, :description, :string
  end
end
