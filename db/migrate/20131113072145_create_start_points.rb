class CreateStartPoints < ActiveRecord::Migration
  def change
    create_table :start_points do |t|
      t.references :map, index: true
      t.integer :x
      t.integer :y

      t.timestamps
    end
  end
end
