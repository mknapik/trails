class AddScoreToAlgorithmStep < ActiveRecord::Migration
  def change
      add_column :algorithm_steps, :score, :integer
  end
end
