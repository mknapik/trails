class AddNameToAlgorithm < ActiveRecord::Migration
  def change
    add_column :algorithms, :name, :string
  end
end
