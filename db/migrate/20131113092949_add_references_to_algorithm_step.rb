class AddReferencesToAlgorithmStep < ActiveRecord::Migration
  def change
    add_column :algorithm_steps, :step_id, :integer
    add_column :algorithm_steps, :algorithm_id, :integer
  end
end
