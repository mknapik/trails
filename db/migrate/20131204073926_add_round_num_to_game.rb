class AddRoundNumToGame < ActiveRecord::Migration
  def change
    add_column :games, :rounds_num, :integer, default: 1, null: false
  end
end
