class CreateAlgorithms < ActiveRecord::Migration
  def change
    create_table :algorithms do |t|
      t.boolean :bot

      t.timestamps
    end
  end
end
